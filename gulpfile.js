var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    pref = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    webserver = require('gulp-webserver'),
    opn = require('opn');

var server = {
    host: '0.0.0.0',
    port: '8081'
};

// Compile sass
gulp.task('sass', function () {
        sass('./scss/*.scss',{
            style: 'compressed'
        })
        .pipe(pref({
            browsers: ['ie >= 9', 'last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./css'));
});


// Watch sass files
gulp.task('watch', ['sass'], function() {
   gulp.watch('./scss/**/*', ['sass']);
});


// Start local webserver
gulp.task('webserver', function() {
    gulp.src( '.' )
        .pipe(webserver({
            host:             server.host,
            port:             server.port,
            livereload:       false,
            directoryListing: false
        }));
});

// Open page in browser
gulp.task('openbrowser', function() {
    opn( 'http://' + server.host + ':' + server.port );
});

gulp.task('default', ['sass', 'webserver', 'openbrowser', 'watch']);

