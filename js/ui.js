/**
 * Created by cyberd on 20.07.16.
 */
function init() {
    $('#header__search__toggler').on('click', function() {
       $(this).parent().toggleClass('header__search--active');
    });
    $('.btn-burger').on('click', function() {
        var self = $(this);
        var target = $( self.data('menu') );

        self.toggleClass('btn-burger--pressed');
        target.toggleClass('show');

    });
}
$(document).ready(init);
